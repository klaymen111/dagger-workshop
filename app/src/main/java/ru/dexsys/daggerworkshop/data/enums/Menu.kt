package ru.dexsys.daggerworkshop.data.enums

enum class Menu(val value: Int) {
    PLATFORM(0),
    ANDROID_STUDIO(1),
    GOOGLE_PLAY(2),
    JETPACK(3),
    KOTLIN(4),
    DOCS(5),
    NEWS(6)
}
