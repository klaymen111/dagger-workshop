package ru.dexsys.daggerworkshop.presentation

import javax.inject.Inject

interface BrowserPresenter {
    fun init(view: BrowserView)
    fun onPageStarted()
    fun onPageFinished()
}

class BrowserPresenterImpl @Inject constructor() : BrowserPresenter {
    private lateinit var view: BrowserView

    override fun init(view: BrowserView) {
        this.view = view
    }

    override fun onPageStarted() {
        view.showLoading()
    }

    override fun onPageFinished() {
        view.hideLoading()
    }
}