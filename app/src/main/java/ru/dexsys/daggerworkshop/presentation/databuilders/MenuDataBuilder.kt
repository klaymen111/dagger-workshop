package ru.dexsys.daggerworkshop.presentation.databuilders

import ru.dexsys.daggerworkshop.R
import ru.dexsys.daggerworkshop.data.enums.Menu
import ru.dexsys.daggerworkshop.presentation.delegates.MenuItem
import ru.dexsys.daggerworkshop.presentation.resourceproviders.MenuResourceProvider
import javax.inject.Inject

interface MenuDataBuilder {
    fun build(): List<MenuItem>
}

class MenuDataBuilderImpl @Inject constructor(private val resourceProvider: MenuResourceProvider) :
    MenuDataBuilder {
    override fun build(): List<MenuItem> = listOf(
        MenuItem(
            menuId = Menu.PLATFORM.value,
            imageRes = R.drawable.ic_android,
            titleText = resourceProvider.platformText,
            url = resourceProvider.platformUrl
        ),
        MenuItem(
            menuId = Menu.ANDROID_STUDIO.value,
            imageRes = R.drawable.ic_baseline_developer_board_24,
            titleText = resourceProvider.androidStudioText,
            url = resourceProvider.androidStudioUrl
        ),
        MenuItem(
            menuId = Menu.GOOGLE_PLAY.value,
            imageRes = R.drawable.ic_baseline_play_arrow_24,
            titleText = resourceProvider.googlePlayText,
            url = resourceProvider.googlePlayUrl
        ),
        MenuItem(
            menuId = Menu.JETPACK.value,
            imageRes = R.drawable.ic_baseline_backpack_24,
            titleText = resourceProvider.jetpackText,
            url = resourceProvider.jetpackUrl
        ),
        MenuItem(
            menuId = Menu.KOTLIN.value,
            imageRes = R.drawable.ic_baseline_developer_mode_24,
            titleText = resourceProvider.kotlinText,
            url = resourceProvider.kotlinUrl
        ),
        MenuItem(
            menuId = Menu.DOCS.value,
            imageRes = R.drawable.ic_baseline_text_snippet_24,
            titleText = resourceProvider.docsText,
            url = resourceProvider.docsUrl
        ),
        MenuItem(
            menuId = Menu.NEWS.value,
            imageRes = R.drawable.ic_baseline_new_releases_24,
            titleText = resourceProvider.newsText,
            url = resourceProvider.newsUrl
        )
    )
}
