package ru.dexsys.daggerworkshop.presentation

import android.annotation.TargetApi
import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import ru.dexsys.daggerworkshop.databinding.ActivityBrowserBinding
import ru.dexsys.daggerworkshop.di.DaggerWrapper
import javax.inject.Inject

interface BrowserView {
    fun hideLoading()
    fun showLoading()
}

class BrowserActivity : AppCompatActivity(), BrowserView {
    private var url: String? = null
    private var _binding: ActivityBrowserBinding? = null
    private val binding get() = _binding!!

    @Inject
    lateinit var presenter: BrowserPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerWrapper.init(this)
        DaggerWrapper.inject(this)
        readArguments()
        _binding = ActivityBrowserBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        val view = binding.root
        setContentView(view)
        presenter.init(this)
        initWebView()
    }

    private fun readArguments() {
        intent.extras.also { bundle ->
            bundle?.let {
                url = (it[Intent.EXTRA_TEXT] as? String) ?: ""
            }
        }
    }

    private fun initWebView() {
        binding.webView.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                presenter.onPageStarted()
                super.onPageStarted(view, url, favicon)
            }

            override fun onPageFinished(view: WebView, url: String) {
                presenter.onPageFinished()
                super.onPageFinished(view, url)
            }

            @TargetApi(Build.VERSION_CODES.N)
            override fun shouldOverrideUrlLoading(
                view: WebView,
                request: WebResourceRequest
            ): Boolean {
                view.loadUrl(request.url.toString())
                return true
            }

            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return true
            }
        }
        binding.webView.settings.javaScriptEnabled = true
        url?.let { binding.webView.loadUrl(it) } ?: onBackPressed()
    }

    override fun onBackPressed() {
        if (binding.webView.canGoBack()) {
            binding.webView.goBack()
        } else {
            super.onBackPressed()
        }
    }

    override fun hideLoading() {
        binding.progress.isVisible = false
    }

    override fun showLoading() {
        binding.progress.isVisible = true
    }
}