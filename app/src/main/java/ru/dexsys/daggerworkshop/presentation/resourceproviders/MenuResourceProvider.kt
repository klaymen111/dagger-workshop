package ru.dexsys.daggerworkshop.presentation.resourceproviders

import android.content.Context
import ru.dexsys.daggerworkshop.R
import javax.inject.Inject

interface MenuResourceProvider {
    val newsUrl: String
    val docsUrl: String
    val kotlinUrl: String
    val jetpackUrl: String
    val googlePlayUrl: String
    val androidStudioUrl: String
    val platformUrl: String
    val newsText: String
    val docsText: String
    val kotlinText: String
    val jetpackText: String
    val googlePlayText: String
    val androidStudioText: String
    val platformText: String
}

class MenuResourceProviderImpl @Inject constructor(private val context: Context) :
    MenuResourceProvider {
    override val newsUrl: String by lazy { context.getString(R.string.news_url) }
    override val docsUrl: String by lazy { context.getString(R.string.docs_url) }
    override val kotlinUrl: String by lazy { context.getString(R.string.kotlin_url) }
    override val jetpackUrl: String by lazy { context.getString(R.string.jetpack_url) }
    override val googlePlayUrl: String by lazy { context.getString(R.string.google_play_url) }
    override val androidStudioUrl: String by lazy { context.getString(R.string.android_studio_url) }
    override val platformUrl: String by lazy { context.getString(R.string.platform_url) }
    override val newsText: String by lazy { context.getString(R.string.news_title) }
    override val docsText: String by lazy { context.getString(R.string.docs_title) }
    override val kotlinText: String by lazy { context.getString(R.string.kotlin_title) }
    override val jetpackText: String by lazy { context.getString(R.string.jetpack_title) }
    override val googlePlayText: String by lazy { context.getString(R.string.google_play_title) }
    override val androidStudioText: String by lazy { context.getString(R.string.android_studio_title) }
    override val platformText: String by lazy { context.getString(R.string.platform_title) }
}
