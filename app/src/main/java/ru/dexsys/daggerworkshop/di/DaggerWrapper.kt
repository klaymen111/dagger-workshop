package ru.dexsys.daggerworkshop.di

import android.content.Context
import ru.dexsys.daggerworkshop.presentation.BrowserActivity
import ru.dexsys.daggerworkshop.presentation.MenuActivity

object DaggerWrapper {
    lateinit var graph: AppGraph

    fun init(context: Context) {
        if (!DaggerWrapper::graph.isInitialized) {
            graph = DaggerAppGraph.builder().plus(context).build()
        }
    }

    fun inject(activity: MenuActivity) {
        graph.inject(activity)
    }

    fun inject(activity: BrowserActivity) {
        graph.inject(activity)
    }
}