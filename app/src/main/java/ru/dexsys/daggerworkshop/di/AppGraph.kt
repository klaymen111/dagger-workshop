package ru.dexsys.daggerworkshop.di

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import ru.dexsys.daggerworkshop.presentation.BrowserActivity
import ru.dexsys.daggerworkshop.presentation.MenuActivity

@Component(modules = [MenuActivityModule::class, BrowserActivityModule::class])
interface AppGraph {
    fun inject(activity: MenuActivity)
    fun inject(activity: BrowserActivity)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun plus(context: Context): Builder
        fun build(): AppGraph
    }
}