package ru.dexsys.daggerworkshop.presentation

import ru.dexsys.daggerworkshop.presentation.databuilders.MenuDataBuilder
import ru.dexsys.daggerworkshop.presentation.delegates.MenuItem
import javax.inject.Inject

interface MenuPresenter {
    fun init(view: MenuView)
    fun onMenuItemClick(item: MenuItem)
}

class MenuPresenterImpl @Inject constructor(private val dataBuilder: MenuDataBuilder) :
    MenuPresenter {
    private lateinit var view: MenuView

    override fun init(view: MenuView) {
        this.view = view
        initState()
    }

    private fun initState() {
        val menuItems = dataBuilder.build()
        view.updateItems(menuItems)
    }

    override fun onMenuItemClick(item: MenuItem) {
        view.openUrl(item.url)
    }
}
