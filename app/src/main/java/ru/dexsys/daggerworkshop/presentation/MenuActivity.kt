package ru.dexsys.daggerworkshop.presentation

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ru.dexsys.daggerworkshop.databinding.ActivityMenuBinding
import ru.dexsys.daggerworkshop.di.DaggerAppGraph
import ru.dexsys.daggerworkshop.di.DaggerWrapper
import ru.dexsys.daggerworkshop.presentation.adapters.DefaultAdapter
import ru.dexsys.daggerworkshop.presentation.databuilders.MenuDataBuilder
import ru.dexsys.daggerworkshop.presentation.databuilders.MenuDataBuilderImpl
import ru.dexsys.daggerworkshop.presentation.delegates.BaseItem
import ru.dexsys.daggerworkshop.presentation.delegates.menuDelegate
import ru.dexsys.daggerworkshop.presentation.resourceproviders.MenuResourceProvider
import javax.inject.Inject

interface MenuView {
    fun updateItems(items: List<BaseItem>)
    fun openUrl(url: String)
}

class MenuActivity : AppCompatActivity(), MenuView {

    @Inject
    lateinit var presenter: MenuPresenter

    private var _binding: ActivityMenuBinding? = null
    private val binding get() = _binding!!

    private val adapter by lazy {
        DefaultAdapter().apply {
            addDelegate(menuDelegate {
                presenter.onMenuItemClick(
                    it
                )
            })
        }
    }
    private val layoutManager by lazy { LinearLayoutManager(this, RecyclerView.VERTICAL, false) }

    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerWrapper.init(this)
        DaggerWrapper.inject(this)
        _binding = ActivityMenuBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        val view = binding.root
        setContentView(view)
        initRecycler()
        presenter.init(this)
    }

    private fun initRecycler() {
        binding.recycler.layoutManager = layoutManager
        binding.recycler.adapter = adapter
    }

    override fun onDestroy() {
        _binding = null
        super.onDestroy()
    }

    override fun updateItems(items: List<BaseItem>) {
        adapter.clear()
        adapter.addItems(items)
        adapter.notifyDataSetChanged()
    }

    override fun openUrl(url: String) {
        val intent = Intent(this, BrowserActivity::class.java).apply {
            putExtra(Intent.EXTRA_TEXT, url)
        }
        startActivity(intent)
    }
}