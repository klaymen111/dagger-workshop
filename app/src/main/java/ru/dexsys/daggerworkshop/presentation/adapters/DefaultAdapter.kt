package ru.dexsys.daggerworkshop.presentation.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import com.hannesdorfmann.adapterdelegates4.AdapterDelegatesManager
import ru.dexsys.daggerworkshop.presentation.delegates.BaseItem
import java.util.*

class DefaultAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val delegatesManager: AdapterDelegatesManager<List<BaseItem>> =
        AdapterDelegatesManager()
    private val items: MutableList<BaseItem> = ArrayList()

    override fun getItemViewType(position: Int): Int {
        return delegatesManager.getItemViewType(items, position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return delegatesManager.onCreateViewHolder(parent, viewType)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        delegatesManager.onBindViewHolder(items, position, holder)
    }

    override fun onViewAttachedToWindow(holder: RecyclerView.ViewHolder) {
        delegatesManager.onViewAttachedToWindow(holder)
    }

    override fun onViewDetachedFromWindow(holder: RecyclerView.ViewHolder) {
        delegatesManager.onViewDetachedFromWindow(holder)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun clear() {
        items.clear()
    }

    fun addItems(items: List<BaseItem>) {
        this.items.addAll(items)
    }

    fun addItem(index: Int, BaseItem: BaseItem) {
        if (index >= 0 && index <= items.size) {
            items.add(index, BaseItem)
        }
    }

    fun addDelegate(delegate: AdapterDelegate<List<BaseItem>>): DefaultAdapter {
        delegatesManager.addDelegate(delegate)
        return this
    }

    fun addDelegates(delegates: List<AdapterDelegate<List<BaseItem>>>) {
        delegates.forEach {
            addDelegate(it)
        }
    }
}