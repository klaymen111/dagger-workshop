package ru.dexsys.daggerworkshop.di

import dagger.Binds
import dagger.Module
import ru.dexsys.daggerworkshop.presentation.MenuPresenter
import ru.dexsys.daggerworkshop.presentation.MenuPresenterImpl
import ru.dexsys.daggerworkshop.presentation.databuilders.MenuDataBuilder
import ru.dexsys.daggerworkshop.presentation.databuilders.MenuDataBuilderImpl
import ru.dexsys.daggerworkshop.presentation.resourceproviders.MenuResourceProvider
import ru.dexsys.daggerworkshop.presentation.resourceproviders.MenuResourceProviderImpl

@Module
interface MenuActivityModule {
    @Binds
    fun bindsResourceProvider(impl: MenuResourceProviderImpl): MenuResourceProvider

    @Binds
    fun bindsDataBuilder(impl: MenuDataBuilderImpl): MenuDataBuilder

    @Binds
    fun bindsPresenter(impl: MenuPresenterImpl): MenuPresenter
}