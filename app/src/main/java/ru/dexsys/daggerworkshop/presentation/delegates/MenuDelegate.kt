package ru.dexsys.daggerworkshop.presentation.delegates

import androidx.annotation.DrawableRes
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding
import ru.dexsys.daggerworkshop.databinding.LayoutMenuItemBinding

internal fun menuDelegate(onClick: (MenuItem) -> Unit) =
    adapterDelegateViewBinding<MenuItem, BaseItem, LayoutMenuItemBinding>(
        { layoutInflater, root ->
            LayoutMenuItemBinding.inflate(
                layoutInflater,
                root,
                false
            )
        }) {
        itemView.setOnClickListener { onClick.invoke(item) }

        bind {
            binding.image.setImageResource(item.imageRes)
            binding.text.text = item.titleText
        }
    }

data class MenuItem(
    val menuId: Int,
    @DrawableRes val imageRes: Int,
    val titleText: String,
    val url: String
) : BaseItem