package ru.dexsys.daggerworkshop.di

import dagger.Binds
import dagger.Module
import ru.dexsys.daggerworkshop.presentation.BrowserPresenter
import ru.dexsys.daggerworkshop.presentation.BrowserPresenterImpl

@Module
interface BrowserActivityModule {
    @Binds
    fun bindsPresenter(impl: BrowserPresenterImpl): BrowserPresenter
}